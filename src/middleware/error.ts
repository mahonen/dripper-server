import winston from 'winston'

export default (error: any, req: any, res: any, next: any) => {
	winston.error(error.message, error)

	res.status(500).send('Something failed!')
}
