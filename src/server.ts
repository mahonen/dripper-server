import winston from 'winston'
import express from 'express'
const app: express.Application = express()
import './init/logging'
import routes from './routes/'

routes(app)

const port: number = parseInt(process.env.PORT) || 8080
if (process.env.NODE_ENV !== 'test') {
	const server = app.listen(port, () =>
		winston.info(`Listening on port ${port}... \n Press CTRL+C to STOP \n`)
	)
}

export default app
