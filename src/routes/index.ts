import express from 'express'
import { Home } from './home'
import { Api } from './api'
import error from '../middleware/error'
import helmet from 'helmet'
import cors from 'cors'

export default (app: express.Application) => {
	app.use(helmet())
	app.use(cors())
	app.use(express.json())
	app.use('/', Home)
	app.use('/api', Api)
	app.use(error)
}