import { Router, Request, Response } from 'express'
import knex from '../db/knex'
import coffeeQuotes from '../quotes/coffee'
const router: Router = Router()

router.get('/coffee', async (req: Request, res: Response) => {
	try {
		// const result = await knex.raw(`select message, cast(extract(epoch from created_at) as int) as ts from dripper`)
		const result = await knex('dripper')
		.select(knex.raw(`message, cast(extract(epoch from created_at) as int) as ts`))
		.distinct().orderBy('ts', 'desc').limit(20)
		return res.status(200).send(result)
	} catch (ex) {
		throw ex
	}
})

router.get('/', async (req: Request, res: Response) => {
	const body = req.body
	const resp = {...body, response: 'success'}
	res.send(resp)
})

router.post('/ready', async (req: Request, res: Response) => {
	const body = req.body
	try {
	const result = await knex('dripper').insert({message: coffeeQuotes()})
	return res.status(200).send(body)
	}

	catch (ex) {
		throw ex
	}})


export const Api: Router = router

const validate = (request: any) => {
	if (request.message && request.ready) {
	if (!request.message || request.ready !== true) {
		return new Error('Message or ready status not provided. Failed to process request.')
	}
	if (request.message.length > 200 || request.message.length < 3) {
		return new Error('Message too long or short, try 3...200')
	}
	return 200
	} else return new Error('Failed')
}
