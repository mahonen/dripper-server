import { Router, Request, Response } from 'express'
const router: Router = Router()

router.get('/', async (req: Request, res: Response) => {
	res.send('hello sirss')
})

router.post('/', async (req: Request, res: Response) => {
	const body = req.body
	res.send(body)
})

export const Home: Router = router
