exports.module = function() {
  const quotes = [
    "Fresh coffee off the pot!",
    "Brew bros, where you at? We be dripping with hot coffee!",
    "Awwww yeaa. Get your cups ready for liquid black gold.",
    "GET EM BOYS. We're serving fresh coffee here.",
    "Tired? No problem, fresh pot, fresh thoughts. Not the 420 kind."
  ];
  const quote = quotes[Math.floor(Math.random() * quotes.length)];
  return quote;
};
