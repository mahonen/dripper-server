import winston from 'winston'
import 'express-async-errors'

export default () => {
	winston.handleExceptions(
		new winston.transports.Console({ colorize: true, prettyPrint: true }),
		new winston.transports.File({ filename: 'uncaughtExceptions.log' })
	)

	process.on('unhandledRejection', ex => {
	throw ex
		})
	winston.add(winston.transports.File, { filename: 'logfile.log' })
}
