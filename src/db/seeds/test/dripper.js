exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('dripper').del()
    .then(function () {
      // Inserts seed entries
      return knex('dripper').insert([
        {id: 1, message: 'First coffee batch is ready!', created_at: '2018-09-04T16:47:06.011Z'},
        {id: 2, message: 'Serving coffee now! Get em boys!', created_at: '2018-09-04T16:12:06.011Z'},
        {id: 3, message: 'The pot is hot, come and get it!', created_at: '2018-09-04T16:33:06.011Z'}
      ]);
    });
};