
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('dripper').del()
    .then(function () {
      // Inserts seed entries
      return knex('dripper').insert([
        {message: 'First coffee batch is ready!'},
        {message: 'Serving coffee now! Get em boys!'},
        {message: 'The pot is hot, come and get it!'}
      ]);
    });
};
