exports.up = function(knex, Promise) {
  let createQuery = `CREATE TABLE dripper(
    id SERIAL PRIMARY KEY NOT NULL,
    message TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
  )`
  return knex.raw(createQuery)
}

exports.down = function(knex, Promise) {
  let dropQuery = `DROP TABLE dripper`
  return knex.raw(dropQuery)
}
