const environment = process.env.NODE_ENV || 'development'
import { development } from '../../knexfile'
import knex from 'knex'

export default knex(development)
