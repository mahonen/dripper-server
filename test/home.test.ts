import request from 'supertest'
import app from '../src/server'

describe('Route /', () => {
	it('should return 200 with GET', async () => {
		await request(app)
			.get('/')
			.expect(200)
	})

	it('should return same object with 200 response with POST', async () => {
		await request(app)
			.post('/')
			.send({ name: 'harri' })
			.set('Accept', 'application/json')
			.expect(200, { name: 'harri' })
	})
})
