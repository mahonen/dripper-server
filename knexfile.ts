// Update with your config settings.

export const development = {
	client: 'pg',
	connection: 'postgres://ana:pass@localhost/dripper',
	migrations: {
		directory: './src/db/migrations'
	},
	seeds: {
		directory: './src/db/seeds/dev'
	},
	useNullAsDefault: true
}

export const test = {
	client: 'pg',
	connection: 'postgres://ana:pass@localhost/dripper_test',
	migrations: {
		directory: './src/db/migrations'
	},
	seeds: {
		directory: './src/db/seeds/test'
	},
	useNullAsDefault: true
}

export const production = {
	client: 'pg',
	connection: process.env.DATABASE_URL,
	migrations: {
		directory: '.src/db/migrations'
	},
	seeds: {
		directory: './src/db/seeds/prod'
	},
	useNullAsDefault: true
}
